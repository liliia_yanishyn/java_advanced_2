package model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
public class Bucket {

    private int id;
    private Timestamp createdDate;

    public Bucket(int id) {
        this.id = id;
        this.createdDate = new Timestamp(System.currentTimeMillis());
    }
}
