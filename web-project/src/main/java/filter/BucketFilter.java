package filter;


import filter.service.FilterService;
import model.Role;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.util.Arrays;

@WebFilter("/bucket.jsp")
public class BucketFilter implements Filter {

    private FilterService service;

    public BucketFilter() {
        service = new FilterService();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        service.doFilterValidation(servletRequest, servletResponse, filterChain,
                Arrays.asList(Role.USER.name()));
    }
}
