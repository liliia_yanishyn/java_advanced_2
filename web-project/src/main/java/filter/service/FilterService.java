package filter.service;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class FilterService {

    public void doFilterValidation(ServletRequest servletRequest, ServletResponse servletResponse,
                                   FilterChain filterChain, List<String> allowedRoles) throws IOException, ServletException {
        try {
            HttpSession session = ((HttpServletRequest) servletRequest).getSession();
            String role = session.getAttribute("role").toString();

            if (!role.isEmpty() && allowedRoles.contains(role)) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                ((HttpServletRequest) servletRequest).getRequestDispatcher("index.jsp").forward(servletRequest, servletResponse);
            }
        } catch (Exception exception) {
            ((HttpServletRequest) servletRequest).getRequestDispatcher("index.jsp").forward(servletRequest, servletResponse);
        }
    }
}
