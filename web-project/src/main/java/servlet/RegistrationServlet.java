package servlet;

import lombok.SneakyThrows;
import model.Role;
import model.User;
import com.logos.service.UserService;
import com.logos.service.impl.UserServiceImpl;
import util.MySQLConnector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

public class RegistrationServlet extends HttpServlet {

    private static UserService service;

    private static Connection connection;

    static {
        try {
            connection = MySQLConnector.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public RegistrationServlet() throws SQLException, ClassNotFoundException {
        service = new UserServiceImpl(connection);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        if (Objects.isNull(service.readByEmail(email))) {
            String firstName = req.getParameter("firstName");
            String lastName = req.getParameter("lastName");
            String password = req.getParameter("password");
            service.create(new User(email, password, firstName, lastName, Role.USER));

            HttpSession session = req.getSession(true);
            session.setAttribute("userName", firstName);
            session.setAttribute("userEmail", email);
        }
        // TODO :redirection to login page
        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write("Success");
    }
}
