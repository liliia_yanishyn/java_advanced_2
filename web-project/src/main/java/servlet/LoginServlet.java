package servlet;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import model.User;
import com.logos.service.UserService;
import com.logos.service.impl.UserServiceImpl;
import util.MySQLConnector;
import util.UserLogin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static UserService service;

    private static Connection connection;

    static {
        try {
            connection = MySQLConnector.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public LoginServlet() throws SQLException, ClassNotFoundException {
        service = new UserServiceImpl(connection);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        User user = service.readByEmail(email);

        if (!Objects.isNull(user)) {
            String password = req.getParameter("password");
            if (password.equals(user.getPassword())) {
                UserLogin userLogin = new UserLogin(email, "cabinet.jsp");

                HttpSession session = req.getSession(true);
                session.setAttribute("userName", user.getFirstName());
                session.setAttribute("userEmail", email);
                session.setAttribute("role", user.getRole());

                String json = new Gson().toJson(userLogin);
                resp.setContentType("application/json");
                resp.setCharacterEncoding("UTF-8");
                resp.getWriter().write(json);
            }
            //TODO: add message that password is not correct . Try again
        }
        //TODO: redirect user to registration
    }
}
