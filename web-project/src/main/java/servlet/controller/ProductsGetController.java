package servlet.controller;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import model.Product;
import org.apache.log4j.Logger;
import com.logos.service.ProductService;
import com.logos.service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/products")
public class ProductsGetController extends HttpServlet {

    private ProductService productService;
    private static final Logger logger = Logger.getLogger(ProductsGetController.class);

    public ProductsGetController() throws SQLException, ClassNotFoundException {
        this.productService = new ProductServiceImpl();
    }

    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Product> products = productService.readAll();
        String jsonData = new Gson().toJson(products);
        logger.info("JSON data : " + jsonData);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write(jsonData);
    }
}
