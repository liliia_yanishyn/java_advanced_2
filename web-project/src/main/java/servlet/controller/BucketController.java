package servlet.controller;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import model.Bucket;
import model.User;
import model.response.ProductResponse;
import com.logos.service.BucketProductService;
import com.logos.service.BucketService;
import com.logos.service.UserService;
import com.logos.service.impl.BucketProductServiceImpl;
import com.logos.service.impl.BucketServiceImpl;
import com.logos.service.impl.UserServiceImpl;
import util.MySQLConnector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

@WebServlet("/bucketInfo")
public class BucketController extends HttpServlet {

    private UserService userService;
    private BucketService bucketService;
    private BucketProductService bucketProductService;

    private static Connection connection;

    static {
        try {
            connection = MySQLConnector.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public BucketController() throws SQLException, ClassNotFoundException {
        userService = new UserServiceImpl(connection);
        bucketService = new BucketServiceImpl();
        bucketProductService = new BucketProductServiceImpl();
    }

    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int bucketId = getUserIdAndBucketIdParameter(req);
        List<ProductResponse> products = bucketProductService.getProductsByBucketId(bucketId);
        String json = new Gson().toJson(products);

        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write(json);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Buy product request");
        int productId = Integer.parseInt(req.getParameter("productId"));
        int bucketId = getUserIdAndBucketIdParameter(req);

        // case if NO such bucket
        if (Objects.isNull(bucketService.read(bucketId))) {
            Bucket bucket = new Bucket(bucketId);
            bucketService.create(bucket);
        }
        bucketProductService.addProductToBucket(bucketId, productId);

        resp.setContentType("text");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write("Success");
    }

    @SneakyThrows
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int productId = Integer.parseInt(req.getParameter("productId"));
        int bucketId = getUserIdAndBucketIdParameter(req);
        boolean allParameter = Boolean.parseBoolean(req.getParameter("all"));

        bucketProductService.removeProductFromBucket(bucketId, productId, allParameter);

        resp.setContentType("text");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write("Success");
    }

    private int getUserIdAndBucketIdParameter(HttpServletRequest req) throws SQLException {
        HttpSession session = req.getSession();
        String email = session.getAttribute("userEmail").toString();
        User user = userService.readByEmail(email);
        return user.getId();
    }
}
