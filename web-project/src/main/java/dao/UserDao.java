package dao;

import exception.AlreadyExistException;
import exception.NotFoundException;
import model.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDao {

    List<User> readAll() throws SQLException;

    User read(int userId) throws SQLException;

    User readByEmail(String email) throws SQLException;

    void create(User user) throws SQLException, AlreadyExistException;

    void update(int previousId, User user) throws SQLException, AlreadyExistException, NotFoundException;
}
