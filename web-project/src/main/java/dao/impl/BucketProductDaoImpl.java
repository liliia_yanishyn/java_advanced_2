package dao.impl;

import dao.BucketProductDao;
import model.BucketProduct;
import model.response.ProductResponse;
import util.MySQLConnector;
import util.QueryUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BucketProductDaoImpl implements BucketProductDao {

    private static Connection connection;

    public BucketProductDaoImpl() throws SQLException, ClassNotFoundException {
        connection = MySQLConnector.getConnection();
    }

    @Override
    public BucketProduct getBucketProductByIds(int bucketId, int productId) throws SQLException {
        ResultSet resultSet = null;
        BucketProduct bucketProduct = null;
        try (PreparedStatement statement = connection.prepareStatement(QueryUtil.GET_BUCKET_PRODUCT)) {
            statement.setInt(1, bucketId);
            statement.setInt(2, productId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                bucketProduct = new BucketProduct(resultSet.getInt("bucket_id"),
                        resultSet.getInt("product_id"),
                        resultSet.getInt("product_count"));
            }
        } finally {
            resultSet.close();
        }
        return bucketProduct;
    }

    @Override
    public List<ProductResponse> getProductsByBucketId(int bucketId) throws SQLException {
        ResultSet resultSet = null;
        List<ProductResponse> products = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(QueryUtil.GET_ALL_PRODUCTS_FROM_BUCKET)) {
            statement.setInt(1, bucketId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                products.add(getProductFromResult(resultSet));
            }
        } finally {
            resultSet.close();
        }
        return products;
    }

    @Override
    public void addProductToBucket(BucketProduct bucketProduct) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(QueryUtil.ADD_PRODUCT_TO_BUCKET)) {
            statement.setInt(1, bucketProduct.getBucketId());
            statement.setInt(2, bucketProduct.getProductId());
            statement.setInt(3, bucketProduct.getProductCount());
            statement.execute();
        }
    }

    @Override
    public void removeProductFromBucket(int bucketId, int productId, boolean all) throws SQLException {
        if (all) {
            try (PreparedStatement statement = connection.prepareStatement(QueryUtil.REMOVE_PRODUCT_FROM_BUCKET)) {
                statement.setInt(1, bucketId);
                statement.setInt(2, productId);
                statement.execute();
            }
        } else {
            int currentCount = getBucketProductByIds(bucketId, productId).getProductCount();
            int productCount = currentCount - 1;
            updateProductCount(productCount, bucketId, productId);
        }
    }

    @Override
    public void updateProductCount(int productCount, int bucketId, int productId) throws SQLException {
        if (productCount == 0) {
            removeProductFromBucket(bucketId, productId, true);
        } else {
            try (PreparedStatement statement = connection.prepareStatement(QueryUtil.UPDATE_PRODUCT_COUNT)) {
                statement.setInt(1, productCount);
                statement.setInt(2, bucketId);
                statement.setInt(3, productId);
                statement.execute();
            }
        }
    }

    private static ProductResponse getProductFromResult(ResultSet resultSet) throws SQLException {
        return new ProductResponse(resultSet.getInt("id"), resultSet.getString("name"),
                resultSet.getString("description"), resultSet.getDouble("price"),
                resultSet.getInt("product_count"));
    }
}
