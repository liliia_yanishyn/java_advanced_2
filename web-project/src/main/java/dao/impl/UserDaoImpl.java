package dao.impl;

import dao.UserDao;
import model.Role;
import model.User;
import util.QueryUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

    private Connection connection;

    public UserDaoImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<User> readAll() throws SQLException {
        List<User> users = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(QueryUtil.GET_ALL_USERS)) {
            while (result.next()) {
                users.add(getUserFromResult(result));
            }
        }
        return users;
    }

    @Override
    public User read(int userId) throws SQLException {
        ResultSet result = null;
        User user = null;
        try (PreparedStatement statement = connection.prepareStatement(QueryUtil.GET_USER)) {
            statement.setInt(1, userId);
            result = statement.executeQuery();
            if (result.next()) {
                user = getUserFromResult(result);
            }
        } finally {
            result.close();
        }
        return user;
    }

    @Override
    public User readByEmail(String email) throws SQLException {
        ResultSet result = null;
        User user = null;
        try (PreparedStatement statement = connection.prepareStatement(QueryUtil.GET_USER_BY_EMAIL)) {
            statement.setString(1, email);
            result = statement.executeQuery();
            if (result.next()) {
                user = getUserFromResult(result);
            }
        } finally {
            result.close();
        }
        return user;
    }

    @Override
    public void create(User user) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(QueryUtil.CREATE_USER)) {
            statement.setInt(1, user.getId());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getRole().name());
            statement.execute();
        }
    }

    @Override
    public void update(int previousId, User user) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(QueryUtil.UPDATE_USER)) {
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getFirstName());
            statement.setString(4, user.getLastName());
            statement.setInt(5, previousId);
            statement.execute();
        }
    }

    private static User getUserFromResult(ResultSet result) throws SQLException {
        User user = new User();
        user.setId(result.getInt("id"));
        user.setEmail(result.getString("email"));
        user.setPassword(result.getString("password"));
        user.setFirstName(result.getString("first_name"));
        user.setLastName(result.getString("last_name"));
        user.setRole(Role.valueOf(result.getString("role")));
        return user;
    }
}
