package dao.impl;

import dao.BucketDao;
import model.Bucket;
import util.MySQLConnector;
import util.QueryUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BucketDaoImpl implements BucketDao {

    private static Connection connection;

    public BucketDaoImpl() throws SQLException, ClassNotFoundException {
        connection = MySQLConnector.getConnection();
    }

    @Override
    public Bucket read(int bucketId) throws SQLException {
        ResultSet resultSet = null;
        Bucket bucket = null;
        try (PreparedStatement statement = connection.prepareStatement(QueryUtil.GET_BUCKET_BY_ID)) {
            statement.setInt(1, bucketId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                bucket = new Bucket(resultSet.getInt("id"), resultSet.getTimestamp("created_date"));
            }
        } finally {
            resultSet.close();
        }
        return bucket;
    }

    @Override
    public void create(Bucket bucket) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(QueryUtil.CREATE_BUCKET)) {
            statement.setInt(1, bucket.getId());
            statement.setTimestamp(2, bucket.getCreatedDate());
            statement.execute();
        }
    }
}
