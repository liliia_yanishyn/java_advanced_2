package dao;

import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Product;

import java.sql.SQLException;
import java.util.List;

public interface ProductDao {

    List<Product> readAll() throws SQLException;

    Product read(int productId) throws SQLException, NotFoundException;

    void create(Product product) throws SQLException, AlreadyExistException, NotFoundException;

}
