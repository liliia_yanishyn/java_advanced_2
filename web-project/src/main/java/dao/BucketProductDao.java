package dao;

import exception.NotFoundException;
import model.BucketProduct;
import model.response.ProductResponse;

import java.sql.SQLException;
import java.util.List;

public interface BucketProductDao {

    BucketProduct getBucketProductByIds(int bucketId, int productId) throws SQLException;

    List<ProductResponse> getProductsByBucketId(int bucketId) throws SQLException, NotFoundException;

    void addProductToBucket(BucketProduct bucketProduct) throws SQLException;

    void removeProductFromBucket(int bucketId, int productId, boolean all) throws SQLException;

    void updateProductCount(int productCount, int bucketId, int productId) throws SQLException;
}
