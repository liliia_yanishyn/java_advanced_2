package dao;

import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Bucket;

import java.sql.SQLException;

public interface BucketDao {

    Bucket read(int bucketId) throws SQLException, NotFoundException;

    void create(Bucket bucket) throws SQLException, AlreadyExistException, NotFoundException;
}
