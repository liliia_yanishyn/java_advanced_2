import exception.AlreadyExistException;
import exception.NotFoundException;
import com.logos.service.BucketProductService;
import com.logos.service.BucketService;
import com.logos.service.ProductService;
import com.logos.service.UserService;
import com.logos.service.impl.BucketProductServiceImpl;
import com.logos.service.impl.BucketServiceImpl;
import com.logos.service.impl.ProductServiceImpl;
import com.logos.service.impl.UserServiceImpl;
import util.MySQLConnector;

import java.sql.SQLException;

public class Main {
    private static UserService service;
    private static ProductService productService;
    private static BucketService bucketService;

    private static BucketProductService bucketProductService;

    static {
        try {
            service = new UserServiceImpl(MySQLConnector.getConnection());
            productService = new ProductServiceImpl();
            bucketService = new BucketServiceImpl();
            bucketProductService = new BucketProductServiceImpl();
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void main(String[] args) throws SQLException, AlreadyExistException, NotFoundException {
        //USER
        //service.readAll().forEach(System.out::println);
        /*System.out.println(service.read(1));
        System.out.println(service.read(11));*/
        //service.create(new User("email", "pass", "fn", "ln", Role.USER));
        //service.update(1673581383, new User("email-2", "pass-2", "fn", "ln", Role.USER));

        //PRODUCT
        //productService.readAll().forEach(System.out::println);
        //System.out.println(productService.read(2));
        //System.out.println(productService.read(22));
        //productService.create(new Product("Banana", "fruit", 25.75));

        //BUCKET
        //bucketService.read(1673581383);
        //bucketService.create(new Bucket(1673581383));

        //BUCKET-PRODUCT
        //bucketProductService.getProductsByBucketId(11).forEach(System.out::println);
        //bucketProductService.addProductToBucket(1673581383, 2222);
        //bucketProductService.removeProductFromBucket(2102542968, 1444169636, false);
    }
}
