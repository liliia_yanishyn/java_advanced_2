package util;

public class QueryUtil {

    //USER
    public static final String GET_ALL_USERS = "SELECT * FROM user";
    public static final String GET_USER = "SELECT * FROM user WHERE id = ?";
    public static final String GET_USER_BY_EMAIL = "SELECT * FROM user WHERE email = ?";
    public static final String CREATE_USER = "INSERT INTO user (id, email, password, first_name, last_name, role) VALUES (?, ?, ?, ?, ?, ?)";
    public static final String UPDATE_USER = "UPDATE user SET email = ?, password = ? , first_name = ?, last_name = ? WHERE id = ?";

    //PRODUCT
    public static final String GET_ALL_PRODUCTS = "SELECT * FROM product ORDER BY name";
    public static final String GET_PRODUCT_BY_ID = "SELECT * FROM product WHERE id = ?";
    public static final String CREATE_PRODUCT = "INSERT INTO product (id, name, description, price) VALUES (?, ?, ?, ?)";

    //BUCKET
    public static final String GET_BUCKET_BY_ID = "SELECT * FROM bucket WHERE id = ?";
    public static final String CREATE_BUCKET = "INSERT INTO bucket (id, created_date) VALUES (?, ?)";

    //BUCKET_PRODUCT
    public static final String ADD_PRODUCT_TO_BUCKET = "INSERT INTO bucket_product (bucket_id, product_id, product_count) VALUES (?, ?, ?)";
    public static final String REMOVE_PRODUCT_FROM_BUCKET = "DELETE FROM bucket_product WHERE bucket_id = ? AND product_id = ?";
    public static final String GET_ALL_PRODUCTS_FROM_BUCKET = "SELECT product.id, product.name, product.description, product.price, bucket_product.product_count " +
            "FROM product INNER JOIN bucket_product ON product.id = bucket_product.product_id WHERE bucket_product.bucket_id = ?";
    public static final String GET_BUCKET_PRODUCT = "SELECT * FROM bucket_product WHERE bucket_id = ? AND product_id = ?";
    public static final String UPDATE_PRODUCT_COUNT = "UPDATE bucket_product SET product_count = ? WHERE (bucket_id = ?) and (product_id = ?)";
}
