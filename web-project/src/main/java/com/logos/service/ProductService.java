package com.logos.service;

import dao.ProductDao;
import exception.NotFoundException;

import java.sql.SQLException;

public interface ProductService extends ProductDao {

    boolean isExists(int productId) throws SQLException, NotFoundException;
}
