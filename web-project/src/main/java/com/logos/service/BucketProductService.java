package com.logos.service;

import exception.NotFoundException;
import model.response.ProductResponse;

import java.sql.SQLException;
import java.util.List;

public interface BucketProductService {

    List<ProductResponse> getProductsByBucketId(int bucketId) throws SQLException, NotFoundException;

    void addProductToBucket(int bucketId, int productId) throws SQLException, NotFoundException;

    void removeProductFromBucket(int bucketId, int productId, boolean all) throws SQLException, NotFoundException;

    boolean isExists(int bucketId, int productId) throws SQLException;
}
