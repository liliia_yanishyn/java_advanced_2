package com.logos.service.impl;

import com.logos.service.BucketService;
import com.logos.service.ProductService;
import dao.BucketProductDao;
import dao.impl.BucketProductDaoImpl;
import exception.NotFoundException;
import model.BucketProduct;
import model.response.ProductResponse;
import org.apache.log4j.Logger;
import com.logos.service.BucketProductService;

import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

public class BucketProductServiceImpl implements BucketProductService {

    private BucketProductDao bucketProductDao;
    private BucketService bucketService;
    private ProductService productService;

    private static final Logger logger = Logger.getLogger(BucketProductServiceImpl.class);

    public BucketProductServiceImpl() throws SQLException, ClassNotFoundException {
        bucketProductDao = new BucketProductDaoImpl();
        bucketService = new BucketServiceImpl();
        productService = new ProductServiceImpl();
    }

    @Override
    public List<ProductResponse> getProductsByBucketId(int bucketId) throws SQLException, NotFoundException {
        logger.info("Request to read all products in bucket : " + bucketId);
        if (bucketService.isExists(bucketId)) {
            return bucketProductDao.getProductsByBucketId(bucketId);
        } else {
            logger.error("No bucket with id : " + bucketId);
            throw new NotFoundException("No bucket with id : " + bucketId);
        }
    }

    @Override
    public void addProductToBucket(int bucketId, int productId) throws SQLException, NotFoundException {
        logger.info("Request to add product : " + productId + " to bucket : " + bucketId);
        if (bucketService.isExists(bucketId) && productService.isExists(productId)) {
            if (isExists(bucketId, productId)) {
                BucketProduct bucketProduct = bucketProductDao.getBucketProductByIds(bucketId, productId);
                int currentCount = bucketProduct.getProductCount();
                int updatedCount = currentCount + 1;
                logger.info("Product : " + productId + " already exists in bucket : "
                        + bucketId + ", product count updated to : " + updatedCount);
                bucketProductDao.updateProductCount(updatedCount, bucketId, productId);
            } else {
                bucketProductDao.addProductToBucket(new BucketProduct(bucketId, productId, 1));
                logger.info("Product : " + productId + " was added to bucket : " + bucketId);
            }
        } else {
            if (!bucketService.isExists(bucketId)) {
                logger.error("No bucket with id : " + bucketId);
                throw new NotFoundException("No bucket with id : " + bucketId);
            } else if (!productService.isExists(productId)) {
                logger.error("No product with id : " + productId);
                throw new NotFoundException("No product with id : " + productId);
            }
        }
    }


    @Override
    public void removeProductFromBucket(int bucketId, int productId, boolean all) throws SQLException, NotFoundException {
        if (bucketService.isExists(bucketId) && productService.isExists(productId)) {
            if (all) {
                logger.info("Request to remove ALL products : " + productId + " from bucket : " + bucketId);
                bucketProductDao.removeProductFromBucket(bucketId, productId, true);
            } else {
                logger.info("Request to remove product : " + productId + " from bucket : " + bucketId);
                bucketProductDao.removeProductFromBucket(bucketId, productId, false);
            }
            logger.info("Product : " + productId + " was deleted from bucket : " + bucketId);
        } else {
            if (!bucketService.isExists(bucketId)) {
                logger.error("No bucket with id : " + bucketId);
                throw new NotFoundException("No bucket with id : " + bucketId);
            } else if (!productService.isExists(productId)) {
                logger.error("No product with id : " + productId);
                throw new NotFoundException("No product with id : " + productId);
            }
        }
    }

    @Override
    public boolean isExists(int bucketId, int productId) throws SQLException {
        return !Objects.isNull(bucketProductDao.getBucketProductByIds(bucketId, productId));
    }
}
