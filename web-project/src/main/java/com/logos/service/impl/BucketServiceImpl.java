package com.logos.service.impl;

import dao.BucketDao;
import dao.impl.BucketDaoImpl;
import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Bucket;
import org.apache.log4j.Logger;
import com.logos.service.BucketService;

import java.sql.SQLException;
import java.util.Objects;

public class BucketServiceImpl implements BucketService {

    private BucketDao bucketDao;
    private static final Logger logger = Logger.getLogger(BucketServiceImpl.class);

    public BucketServiceImpl() throws SQLException, ClassNotFoundException {
        bucketDao = new BucketDaoImpl();
    }

    @Override
    public Bucket read(int bucketId) throws SQLException, NotFoundException {
        Bucket bucket = bucketDao.read(bucketId);
        if (Objects.isNull(bucket)) {
            logger.info("Bucket with id : " + bucketId + " not found");
            return null;
        } else {
            logger.info("Getting bucket with id : " + bucketId);
            logger.info(bucket);
            return bucket;
        }
    }

    @Override
    public void create(Bucket bucket) throws SQLException, AlreadyExistException, NotFoundException {
        if (isExists(bucket.getId())) {
            logger.error("Bucket with id : " + bucket.getId() + " already exists and can`t be created");
            throw new AlreadyExistException("Bucket with id : " + bucket.getId() + " already exists");
        } else {
            logger.info("Creating bucket : " + bucket);
            bucketDao.create(bucket);
        }
    }

    @Override
    public boolean isExists(int bucketId) throws SQLException, NotFoundException {
        return !Objects.isNull(bucketDao.read(bucketId));
    }
}
