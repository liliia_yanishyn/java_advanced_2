package com.logos.service.impl;

import com.logos.service.UserService;
import dao.UserDao;
import dao.impl.UserDaoImpl;
import exception.AlreadyExistException;
import exception.NotFoundException;
import model.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;


public class UserServiceImpl implements UserService {

    private static UserDao userDao;

    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);

    public UserServiceImpl(Connection connection) throws SQLException, ClassNotFoundException {
        userDao = new UserDaoImpl(connection);
    }

    @Override
    public List<User> readAll() throws SQLException {
        LOG.info("Request to GET all users created!");
        return userDao.readAll();
    }

    @Override
    public User read(int userId) throws SQLException {
        LOG.info("Request to GET user by id = " + userId + " created!");
        User user = userDao.read(userId);
        LOG.info("USER : " + user);
        return user;
    }

    @Override
    public User readByEmail(String email) throws SQLException {
        LOG.info("Request to GET user by email = " + email + " created!");
        User user = userDao.readByEmail(email);
        LOG.info("USER : " + user);
        return user;
    }

    @Override
    public void create(User user) throws SQLException, AlreadyExistException {
        LOG.info("Request to POST user :  " + user + " created!");
        if (!isExists(user.getId()) && Objects.isNull(userDao.readByEmail(user.getEmail()))) {
            userDao.create(user);
            LOG.info("User created successfully!");
        } else {
            LOG.error("USER with id = " + user.getId() + " already exists OR email " + user.getEmail() + " already exists");
            throw new AlreadyExistException("USER with id = " + user.getId() + " already exists OR email " + user.getEmail() + " already exists");
        }
    }

    @Override
    public void update(int previousId, User user) throws SQLException, AlreadyExistException, NotFoundException {
        LOG.info("Request to PUT user with id : " + previousId + " to new user : " + user + "created!");
        if (isExists(previousId)) {
            userDao.update(previousId, user);
            LOG.info("User updated successfully!");
        } else {
            LOG.error("USER with id = " + previousId + " does not exists, so cannot be updated");
            throw new NotFoundException("User with id = " + previousId + " does not exists, so cannot be updated");
        }
    }

    private boolean isExists(int userId) throws SQLException {
        return !Objects.isNull(userDao.read(userId));
    }
}
