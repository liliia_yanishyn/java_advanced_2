package com.logos.service.impl;

import com.logos.service.ProductService;
import dao.ProductDao;
import dao.impl.ProductDaoImpl;
import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Product;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

public class ProductServiceImpl implements ProductService {

    private ProductDao productDao;
    private static final Logger logger = Logger.getLogger(ProductServiceImpl.class);

    public ProductServiceImpl() throws SQLException, ClassNotFoundException {
        productDao = new ProductDaoImpl();
    }

    @Override
    public List<Product> readAll() throws SQLException {
        logger.info("Read all products request");
        return productDao.readAll();
    }

    @Override
    public Product read(int productId) throws SQLException, NotFoundException {
        Product product = productDao.read(productId);
        if (Objects.isNull(product)) {
            logger.error("Product with id : " + productId + " not found");
            throw new NotFoundException("Product with id : " + productId + " not found");
        } else {
            logger.info("Getting product with id : " + productId);
            logger.info(product);
            return product;
        }
    }

    @Override
    public void create(Product product) throws SQLException, AlreadyExistException, NotFoundException {
        if (isExists(product.getId())) {
            logger.error("Product with id : " + product.getId() + " already exists and can`t be created");
            throw new AlreadyExistException("Product with id : " + product.getId() + " already exists");
        } else {
            logger.info("Creating product : " + product);
            productDao.create(product);
        }
    }

    @Override
    public boolean isExists(int productId) throws SQLException, NotFoundException {
        return !Objects.isNull(productDao.read(productId));
    }
}
