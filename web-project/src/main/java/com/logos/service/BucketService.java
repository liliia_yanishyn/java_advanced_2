package com.logos.service;

import dao.BucketDao;
import exception.NotFoundException;

import java.sql.SQLException;

public interface BucketService extends BucketDao {

    boolean isExists(int bucketId) throws SQLException, NotFoundException;
}
