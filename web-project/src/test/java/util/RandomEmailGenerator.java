package util;

import java.util.Random;

public class RandomEmailGenerator {

    private static final String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    public static String getRandomEmail() {
        StringBuilder email = new StringBuilder();
        Random random = new Random();
        while (email.length() < 10) {
            int index = (int) (random.nextFloat() * CHARS.length());
            email.append(CHARS.charAt(index));
        }
        return email.toString() + "@gmail.com";
    }
}
