package com.logos.service.impl;

import com.logos.service.UserService;
import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Role;
import model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import util.MySQLConnectorTest;
import util.RandomEmailGenerator;
import util.RandomIdGenerator;

import java.sql.SQLException;

public class UserServiceTest {

    private static UserService userService;

    @BeforeAll
    public static void init() throws SQLException, ClassNotFoundException {
        userService = new UserServiceImpl(MySQLConnectorTest.getConnection());
    }

    @Test
    @DisplayName("GET user and POST user tests")
    public void readUserByIdTest() throws SQLException, AlreadyExistException {
        int id = RandomIdGenerator.getRandomID();
        String email = RandomEmailGenerator.getRandomEmail();
        User expected = new User(id, email, "qwerty123", "Anna", "Lalal", Role.USER);

        userService.create(expected);
        User actualById = userService.read(id);

        // check by ID
        Assertions.assertEquals(actualById, expected);

        User actualByEmail = userService.readByEmail(email);

        // check by Email
        Assertions.assertEquals(actualByEmail, expected);
    }

    @Test
    @DisplayName("POST user with Exception test")
    public void createUserWithExceptionTest() throws SQLException, AlreadyExistException {
        int id = RandomIdGenerator.getRandomID();
        String email = RandomEmailGenerator.getRandomEmail();
        User expected = new User(id, email, "qwerty123", "Anna", "Lalal", Role.USER);

        userService.create(expected);
        Assertions.assertThrows(AlreadyExistException.class, () -> userService.create(expected));
    }

    @Test
    @DisplayName("UPDATE user test")
    public void updateUserTest() throws SQLException, AlreadyExistException, NotFoundException {
        int id = RandomIdGenerator.getRandomID();
        String email = RandomEmailGenerator.getRandomEmail();
        User user = new User(id, email, "qwerty123", "Anna", "Lalal", Role.USER);

        userService.create(user);

        String updatedEmail = RandomEmailGenerator.getRandomEmail();
        User userToUpdate = new User(updatedEmail, "password", "Ivan", "Surname", Role.USER);

        userService.update(id, userToUpdate);
        User expected = new User(id, updatedEmail, "password", "Ivan", "Surname", Role.USER);
        Assertions.assertEquals(expected, userService.readByEmail(updatedEmail));
    }

    @Test
    @DisplayName("UPDATE user test with NotFoundException")
    public void updateUserNotFoundExceptionTest() {
        String updatedEmail = RandomEmailGenerator.getRandomEmail();
        User userToUpdate = new User(updatedEmail, "password", "Ivan", "Surname", Role.USER);

        Assertions.assertThrows(NotFoundException.class, () -> userService.update(0, userToUpdate));
    }
}
