package com.logos.service;

import com.logos.exception.CannotDivideIntoNullException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

public class CalculationsImplTest {

    private static Calculations calculations;

    @BeforeAll
    public static void init() {
        calculations = new CalculationsImpl();
    }

    @BeforeEach
    public void start() {
        System.out.println("Test started");
    }

    @AfterEach
    public void finish() {
        System.out.println("Test finished");
    }

    @Test
    @DisplayName("Test sum() method")
    public void sumTest() {
        double actual = calculations.sum(5, 1);
        double expected = 6;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test minus() method")
    public void minusTest() {
        double actual = calculations.minus(5, -10);
        double expected = 15;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test multiply() method")
    public void multiplyTest() {
        double actual = calculations.multiply(5, -2);
        double expected = -10;
        Assertions.assertEquals(expected, actual);
    }


    @ParameterizedTest
    @CsvSource({
            "5, 2, 10",
            "25, 4 , 100",
            "7, 3, 21",
            "5, 0, 0"
    })
    @DisplayName("Test multiply() method - parametrized")
    public void multiplyTestParametrized(double n1, double n2, double expected) {
        double actual = calculations.multiply(n1, n2);
        Assertions.assertEquals(expected, actual);
    }


    @Test
    @DisplayName("Test divide() method")
    public void divideTest() throws CannotDivideIntoNullException {
        double actual = calculations.divide(5, -1);
        double expected = -5;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test divide() method --- with error")
    public void divideTestThrowError() throws CannotDivideIntoNullException {
        Assertions.assertThrows(CannotDivideIntoNullException.class,
                () -> calculations.divide(5, 0));
    }

    @Test
    @DisplayName("Test isPositive() method - TRUE")
    public void isPositiveTest() {
        Assertions.assertTrue(calculations.isPositive(25));
    }

    @Test
    @DisplayName("Test isPositive() method - FALSE")
    public void isPositiveTestWithFalse() {
        Assertions.assertFalse(calculations.isPositive(-956));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 25, -8, 0, -63, 233, 152222})
    @DisplayName("Test isPositive() parametrized tests")
    public void isPositiveTestParametrized(int number) {
        Assertions.assertTrue(calculations.isPositive(number));
    }

}
