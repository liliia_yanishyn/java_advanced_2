import many_to_many.Team;
import many_to_many.User;
import one_to_many.Cart;
import one_to_many.Item;
import one_to_one.Customer;
import one_to_one.FinancialOperation;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.SessionFactoryUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Application {

    private static final Session session = SessionFactoryUtil.getSession();
    private static final Transaction transaction = session.beginTransaction();

    public static void main(String[] args) {
        //oneToOneExample();
        //oneToManyExample();
        //manyToManyExample();
    }

    private static void manyToManyExample() {
        Team team1 = new Team("team-1");
        Team team2 = new Team("team-2");

        User user1 = new User("email-1", "pass-1");
        User user2 = new User("email-2", "pass-2");

        team1.addUser(user1);
        team1.addUser(user2);
        user1.addTeam(team1);
        user2.addTeam(team1);

        team2.addUser(user2);
        user2.addTeam(team2);

        session.save(team1);
        session.save(team2);

        transaction.commit();
    }

    private static void oneToManyExample() {
        Cart cart1 = new Cart("cart-1");
        Cart cart2 = new Cart("cart-2");

        Item item1 = new Item(147, new Date(System.currentTimeMillis()), cart1);
        Item item2 = new Item(52, new Date(System.currentTimeMillis()), cart1);
        Item item3 = new Item(18, new Date(System.currentTimeMillis()), cart2);
        Item item4 = new Item(95, new Date(System.currentTimeMillis()), cart2);

        Set<Item> set1 = new HashSet<>(Arrays.asList(item1, item2));
        cart1.setItems(set1);

        Set<Item> set2 = new HashSet<>(Arrays.asList(item3, item4));
        cart2.setItems(set2);

        session.save(cart1);
        session.save(cart2);

        transaction.commit();
    }

    private static void oneToOneExample() {
        Customer customer = new Customer("Liliia Yanishyn", "liliya_7@yahoo.com");
        FinancialOperation operation = new FinancialOperation(new Date(System.currentTimeMillis()), 2367);

        customer.setFinancialOperation(operation);

        session.save(customer);

        Customer customerFromDB = session.get(Customer.class, 1);
        System.out.println(customerFromDB);

        FinancialOperation operationFromDB = session.get(FinancialOperation.class, 2);
        System.out.println(operationFromDB);

        transaction.commit();
    }


}
