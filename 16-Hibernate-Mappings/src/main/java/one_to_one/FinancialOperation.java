package one_to_one;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "financial_operation")
public class FinancialOperation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "operation_id", unique = true, nullable = false)
    private int id;

    @Column(name = "operation_date")
    private Date date;

    @Column(name = "total")
    private double total;

    @OneToOne(mappedBy = "financialOperation")
    private Customer customer;

    public FinancialOperation(Date date, double total) {
        this.date = date;
        this.total = total;
    }

    @Override
    public String toString() {
        return "FinancialOperation{" +
                "id=" + id +
                ", date=" + date +
                ", total=" + total +
                '}';
    }
}
