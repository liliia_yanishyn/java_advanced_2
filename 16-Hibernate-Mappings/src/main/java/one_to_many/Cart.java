package one_to_many;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cart")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cart_id", unique = true, nullable = false)
    private int id;

    @Column(name = "cart_name")
    private String cartName;

    @Column(name = "tatal_price")
    private double totalPrice;

    @OneToMany(mappedBy = "cart", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Item> items;

    public Cart(String cartName) {
        this.cartName = cartName;
    }

    public void setItems(Set<Item> itemSet) {
        this.items = itemSet;
        this.totalPrice = getTotalPrice(itemSet);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cart cart = (Cart) o;
        return id == cart.id &&
                Double.compare(cart.totalPrice, totalPrice) == 0 &&
                cartName.equals(cart.cartName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cartName, totalPrice);
    }

    private double getTotalPrice(Set<Item> items) {
        return items.stream().mapToDouble(Item::getPrice).sum();
    }
}
