package com.logos.lesson20.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MultipartUploadResponse {

    private String fileName;
    private String downloadUrl;
    private String fileType;
    private long size;
}
