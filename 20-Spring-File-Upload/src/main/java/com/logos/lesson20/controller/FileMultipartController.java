package com.logos.lesson20.controller;

import com.logos.lesson20.model.request.FileMultipart;
import com.logos.lesson20.model.response.MultipartUploadResponse;
import com.logos.lesson20.service.impl.FileMultipartServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FileMultipartController {

    @Autowired
    private FileMultipartServiceImpl service;

    @PostMapping("/uploadFile")
    private MultipartUploadResponse uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        FileMultipart fileMultipart = service.saveFile(file);
        String fileDownloadUrl = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
                .path(fileMultipart.getId()).toUriString();
        return new MultipartUploadResponse(fileMultipart.getFileName(), fileDownloadUrl,
                file.getContentType(), file.getSize());
    }

    @PostMapping("/uploadMultipleFiles")
    public List<MultipartUploadResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.stream(files).map(file -> {
            try {
                return uploadFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }).collect(Collectors.toList());
    }

    @GetMapping("/downloadFile/{id}")
    public ResponseEntity<?> downloadFile(@PathVariable("id") String id) throws FileNotFoundException {
        FileMultipart fileMultipart = service.getFile(id);
        return ResponseEntity.ok().contentType(MediaType.parseMediaType(fileMultipart.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment ; filename=\"" + fileMultipart.getFileName() + "\"")
                .body(new ByteArrayResource(fileMultipart.getData()));
    }
}
