package com.logos.lesson20.repository;

import com.logos.lesson20.model.request.FileMultipart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileMultipartRepository extends JpaRepository<FileMultipart, String> {
}
