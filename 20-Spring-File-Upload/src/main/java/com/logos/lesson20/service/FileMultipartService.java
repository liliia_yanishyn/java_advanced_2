package com.logos.lesson20.service;

import com.logos.lesson20.model.request.FileMultipart;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileMultipartService {

    FileMultipart saveFile(MultipartFile file) throws IOException;

    FileMultipart getFile(String fileId) throws FileNotFoundException;
}
