let multipleFileUploadForm = document.querySelector("#multipleFileUploadForm");
let multipleFileUploadInput = document.querySelector("#multipleFileUploadInput");
let multipleFileUploadSuccess = document.querySelector("#multipleFileUploadSuccess");
let multipleFileUploadError = document.querySelector("#multipleFileUploadError");


multipleFileUploadForm.addEventListener('submit', function (event) {
    let files = multipleFileUploadInput.files;
    if (files.length === 0) {
        multipleFileUploadError.innerHTML = "Please select at least one file!";
        multipleFileUploadError.style.color = "red";
    }
    uploadMultipleFiles(files);
    event.preventDefault()
}, true);


function uploadMultipleFiles(files) {
    let data = new FormData();
    for (let i = 0; i < files.length; i++) {
        data.append("files", files[i]);
    }

    let xhr = new XMLHttpRequest();
    xhr.open("POST", "/uploadMultipleFiles");

    xhr.onload = function () {
        let response = JSON.parse(xhr.responseText);
        console.log(response);

        if (xhr.status === 200) {
            multipleFileUploadError.style.display = "none";
            let content = "<p>All files uploaded successfully</p>";
            for (let i = 0; i < response.length; i++) {
                content += "<p>Download url : <a href='" + response[i].downloadUrl +
                    "' target='_blank'>"
                    + response[i].downloadUrl
                    + "</a></p>";
            }
            console.log(content);
            multipleFileUploadSuccess.style.display = "block";
            multipleFileUploadSuccess.innerHTML = content;
        } else {
            multipleFileUploadSuccess.style.display = "none";
            multipleFileUploadError.innerHTML = response.message || "Some error occured";
        }
    };
    xhr.send(data);
}