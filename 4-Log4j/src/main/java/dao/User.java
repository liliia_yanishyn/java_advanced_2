package dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User {

    private int id;
    private String fullName;
    private int age;
    private String address;
    private String dateOfBirth;

    public User(String fullName, int age, String address, String dateOfBirth) {
        this.fullName = fullName;
        this.age = age;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
    }
}
