package util;

public class QueryUtil {

    public static final String GET_ALL_USERS = "SELECT * FROM user";
    public static final String GET_USER_BY_ID = "SELECT * FROM user WHERE user_id = ?";
    public static final String CREATE_USER = "INSERT INTO user (user_id, user_full_name, user_age, user_address, user_date_of_birth) VALUES (?, ?, ?, ?, ?)";
    public static final String UPDATE_USER = "UPDATE user SET user_full_name = ? , user_age = ?, user_address = ?, user_date_of_birth = ? WHERE user_id = ?";
    public static final String DELETE_USER = "DELETE FROM user WHERE user_id = ?";

    public static final String GET_USERS_BY_AGE_AND_SORTED = "SELECT * FROM user WHERE user_age >= ? ORDER BY user_age ASC";
}
