package com.logos.service;

import dao.User;
import exception.DuplicateUserException;
import exception.NoSuchUserException;
import org.apache.log4j.Logger;
import util.MySQLConnector;
import util.QueryUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class UserServiceImpl implements UserService {

    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);
    private static Connection connection;

    static {
        try {
            connection = MySQLConnector.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> readAll() throws SQLException {
        List<User> users = new ArrayList<>();
        logger.info("Request to read all users!");
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(QueryUtil.GET_ALL_USERS)) {
            while (result.next()) {
                User user = getUserFromResult(result);
                users.add(user);
                logger.debug("USER : " + user);
            }
        }
        return users;
    }

    @Override
    public List<User> getUsersByAgeSorted(int userAgeMoreThan) throws SQLException {
        List<User> users = new LinkedList<>();
        ResultSet result = null;
        logger.info("Request to get all users older than " + userAgeMoreThan + " years sorted");
        try (PreparedStatement statement = connection.prepareStatement(QueryUtil.GET_USERS_BY_AGE_AND_SORTED)) {
            statement.setInt(1, userAgeMoreThan);
            result = statement.executeQuery();
            while (result.next()) {
                User user = getUserFromResult(result);
                users.add(user);
                logger.debug("USER : " + user);
            }
        } finally {
            result.close();
        }
        return users;
    }

    @Override
    public User read(int userId) throws SQLException, NoSuchUserException {
        ResultSet result = null;
        logger.info("Request to read user with id : " + userId);
        try (PreparedStatement statement = connection.prepareStatement(QueryUtil.GET_USER_BY_ID)) {
            statement.setInt(1, userId);
            result = statement.executeQuery();
            if (result.next()) {
                User user = getUserFromResult(result);
                logger.debug("USER : " + user);
                return user;
            } else {
                logger.error("No user with id : " + userId);
                throw new NoSuchUserException("No user with id : " + userId);
            }
        } finally {
            result.close();
        }
    }

    @Override
    public void create(User user) throws SQLException, DuplicateUserException {
        logger.info("Request to create user : " + user);
        if (!isExists(user.getId())) {
            try (PreparedStatement statement = connection.prepareStatement(QueryUtil.CREATE_USER)) {
                statement.setInt(1, user.getId());
                statement.setString(2, user.getFullName());
                statement.setInt(3, user.getAge());
                statement.setString(4, user.getAddress());
                statement.setString(5, user.getDateOfBirth());
                statement.execute();
                logger.info("User : " + user + " was created");
            }
        } else {
            logger.error("User with id : " + user.getId() + " already exists");
            throw new DuplicateUserException("User with id : " + user.getId() + " already exists");
        }
    }

    @Override
    public void update(int previousId, User newUser) throws SQLException, NoSuchUserException {
        logger.info("Request to update user with id : " + previousId + " to new user : " + newUser);
        if (isExists(previousId)) {
            try (PreparedStatement statement = connection.prepareStatement(QueryUtil.UPDATE_USER)) {
                statement.setString(1, newUser.getFullName());
                statement.setInt(2, newUser.getAge());
                statement.setString(3, newUser.getAddress());
                statement.setString(4, newUser.getDateOfBirth());
                statement.setInt(5, previousId);
                statement.execute();
                logger.info("User with id : " + previousId + " was successfully updated");
            }
        } else {
            logger.error("No user with id : " + previousId);
            throw new NoSuchUserException("No user with id : " + previousId);
        }
    }

    @Override
    public void delete(int userId) throws NoSuchUserException, SQLException {
        logger.info("Request to delete user with id : " + userId);
        if (isExists(userId)) {
            try (PreparedStatement statement = connection.prepareStatement(QueryUtil.DELETE_USER)) {
                statement.setInt(1, userId);
                statement.execute();
                logger.info("User with id : " + userId + " was successfully deleted");
            }
        } else {
            logger.error("No user with id : " + userId);
            throw new NoSuchUserException("No user with id : " + userId);
        }
    }

    private boolean isExists(int userId) throws SQLException {
        boolean flag = false;
        for (User user : readAllUsers()) {
            if (user.getId() == userId) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    private static User getUserFromResult(ResultSet result) throws SQLException {
        User user = new User();
        user.setId(result.getInt("user_id"));
        user.setFullName(result.getString("user_full_name"));
        user.setAge(result.getInt("user_age"));
        user.setAddress(result.getString("user_address"));
        user.setDateOfBirth(result.getString("user_date_of_birth"));
        return user;
    }

    public static List<User> readAllUsers() throws SQLException {
        List<User> users = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(QueryUtil.GET_ALL_USERS)) {
            while (result.next()) {
                User user = getUserFromResult(result);
                users.add(user);
            }
        }
        return users;
    }
}
