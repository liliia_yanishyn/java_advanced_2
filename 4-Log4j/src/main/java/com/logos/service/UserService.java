package com.logos.service;

import dao.User;
import exception.DuplicateUserException;
import exception.NoSuchUserException;

import java.sql.SQLException;
import java.util.List;

public interface UserService {

    List<User> readAll() throws SQLException;

    List<User> getUsersByAgeSorted(int userAgeMoreThan) throws SQLException;

    User read(int userId) throws SQLException, NoSuchUserException;

    void create(User user) throws SQLException, DuplicateUserException;

    void update(int previousId, User newUser) throws SQLException, NoSuchUserException;

    void delete(int userId) throws NoSuchUserException, SQLException;
}
