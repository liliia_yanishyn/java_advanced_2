CREATE DATABASE IF NOT EXISTS user_data;

DROP DATABASE IF EXISTS user_data;

USE user_data;

CREATE TABLE IF NOT EXISTS user (
	user_full_name VARCHAR(100),
    user_age TINYINT,
    user_address TINYTEXT,
    user_date_of_birth DATETIME
);

DROP TABLE IF EXISTS user;

SELECT * FROM user;

ALTER TABLE user ADD COLUMN user_id TINYINT AFTER user_full_name;

ALTER TABLE user ADD COLUMN user_id TINYINT FIRST;

ALTER TABLE user DROP COLUMN user_id;

ALTER TABLE user ADD PRIMARY KEY(user_id);

ALTER TABLE user MODIFY COLUMN user_date_of_birth DATE;


INSERT INTO user (user_id, user_full_name, user_age, user_address, user_date_of_birth)
VALUES
(2, 'Petro', 45, 'Poltava', '1972-03-23'),
(3, 'Anna', 30, 'Odessa', '1990-11-22'),
(4, 'Oleg', 16, 'Ternopil', '2004-12-02');