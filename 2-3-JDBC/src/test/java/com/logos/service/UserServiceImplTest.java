package com.logos.service;

import dao.User;
import exception.DuplicateUserException;
import exception.NoSuchUserException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

public class UserServiceImplTest {

    private static UserService service;
    private static final int ID = 0;

    @BeforeAll
    public static void init() {
        service = new UserServiceImpl();
    }

    @Test
    @DisplayName("Test create user object")
    public void createUserTest() throws SQLException, DuplicateUserException, NoSuchUserException {
        User expected = getUser();
        service.create(expected);

        User actual = service.read(ID);
        Assertions.assertEquals(expected, actual);

        service.delete(ID);
        Assertions.assertThrows(NoSuchUserException.class,
                () -> service.read(ID));
    }

    @Test
    @DisplayName("Test create user object with DuplicateUserException")
    public void createUserWithExceptionTest() throws SQLException, DuplicateUserException, NoSuchUserException {
        User expected = getUser();
        service.create(expected);

        Assertions.assertThrows(DuplicateUserException.class,
                () -> service.create(expected));

        service.delete(ID);
    }

    @Test
    @DisplayName("Test delete user object with NoSuchUserException")
    public void deleteUserWithExceptionTest() throws SQLException, DuplicateUserException, NoSuchUserException {
        User user = getUser();
        service.create(user);
        service.delete(ID);

        Assertions.assertThrows(NoSuchUserException.class,
                () -> service.delete(ID));
    }

    @Test
    @DisplayName("Test update user object")
    public void updateUserTest() throws SQLException, DuplicateUserException, NoSuchUserException {
        User user = getUser();
        service.create(user);

        User expected = getUserToUpdate();
        service.update(ID, expected);

        User actual = service.read(ID);
        Assertions.assertEquals(expected, actual);

        service.delete(ID);
    }

    @Test
    @DisplayName("Test update user object with NoSuchUserException")
    public void updateUserWithExceptionTest() {
        User newUser = getUserToUpdate();
        Assertions.assertThrows(NoSuchUserException.class,
                () -> service.update(ID, newUser));
    }

    private static User getUser() {
        return new User(ID, "Name Surname", 123, "Address", "1995-12-05");
    }

    private static User getUserToUpdate() {
        return new User("UPDATED", 75, "Address - NEW", "2000-12-13");
    }

}
