package ua.lviv.lgs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.lviv.lgs.model.request.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, String> {

}
