package ua.lviv.lgs.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.lviv.lgs.model.request.Student;

import java.util.Base64;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentUploadResponse {

    private String name;
    private String surname;
    private int age;
    private String base64Image;

    public StudentUploadResponse(Student student) {
        this.name = student.getFirstName();
        this.surname = student.getLastName();
        this.age = student.getAge();
        this.base64Image = Base64.getEncoder().encodeToString(student.getPhoto().getData());
    }
}