package ua.lviv.lgs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ua.lviv.lgs.model.request.Photo;
import ua.lviv.lgs.model.request.Student;
import ua.lviv.lgs.model.response.StudentUploadResponse;
import ua.lviv.lgs.service.StudentService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping("/register")
    public StudentUploadResponse registerStudent(@RequestParam("photo") MultipartFile file, HttpServletRequest req,
                                                 HttpServletResponse response) throws IOException, ServletException {
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        int age = Integer.parseInt(req.getParameter("age"));

        String photoName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String type = file.getContentType();
        byte[] data = file.getBytes();

        Photo photo = new Photo(photoName, type, data);

        Student student = new Student(name, surname, age, photo);
        photo.setStudent(student);
        //student.setPhoto(photo);
        studentService.save(student);

        return new StudentUploadResponse(student);
    }
}