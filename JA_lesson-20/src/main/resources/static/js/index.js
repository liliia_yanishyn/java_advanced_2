$('form').submit(function (e) {
    e.preventDefault();
    if (validateForm() === true) {
        let photo = $("#photo");
        let formData = new FormData;

        formData.append('photo', photo.prop('files')[0]);
        formData.append('name', $('#name').val());
        formData.append('surname', $('#surname').val());
        formData.append('age', $('#age').val());

        $.ajax({
            url: 'register',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (student) {
                $('form').hide();
                $('div.show_photo')
                    .html(
                        '<h1>Registered student</h1>' +
                        '<h2>First name - ' + student.name + '</h2>' +
                        '<h2>Last name - ' + student.surname + '</h2>' +
                        '<h3>Age - ' + student.age + ' years</h3>' +
                        '<h4>Students photo<h4>' +
                        '<img src="" /> ');
                $('img').attr(
                    "src",
                    'data:image/png;base64,'
                    + student.base64Image);
                $('div.show_photo').show();
            }
        });

    }
});

function validateForm() {
    let name = document.forms["my-form"]["name"].value;
    let surname = document.forms["my-form"]["surname"].value;
    let age = document.forms["my-form"]["age"].value;
    let photo = document.forms["my-form"]["photo"].value;

    if (name == null || name == "") {
        alert("Please enter your name");
        return false;
    } else if (surname == null || surname == "") {
        alert("Please enter your surname");
        return false;
    } else if (age == null || age == "") {
        alert("Please enter your age");
        return false;
    } else if (photo == null || photo == "") {
        alert("Please load your photo");
        return false;
    }
    return true;
}