package com.logos.lesson17.dao.impl;

import com.logos.lesson17.dao.ShopDao;
import com.logos.lesson17.model.Shop;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ShopDaoImpl implements ShopDao {

    private List<Shop> shopTable;

    public ShopDaoImpl() {
        shopTable = new ArrayList<>();
    }

    @Override
    public List<Shop> readAll() {
        return shopTable;
    }

    @Override
    public Shop read(int id) {
        return shopTable.stream().filter(shop -> shop.getId() == id).findFirst()
                .orElseThrow(NullPointerException::new);
    }

    @Override
    public void create(Shop shop) {
        if (!shopTable.contains(shop)) shopTable.add(shop);
        else System.out.println("Shop already exists");
    }

    @Override
    public void update(int idPrevious, String property, String newValue) {
        Shop shop = read(idPrevious);
        int index = shopTable.indexOf(shop);
        switch (property) {
            case "name": {
                shop.setName(newValue);
                break;
            }
            case "square": {
                shop.setSquare(Double.parseDouble(newValue));
                break;
            }
            case "maxPeople": {
                shop.setMaxPeople(Integer.parseInt(newValue));
                break;
            }
            default: {
                System.out.println("Wrong property name!");
                break;
            }
        }
        shopTable.set(index, shop);
    }

    @Override
    public void updateFull(int idPrevious, Shop shop) {
        Shop shopCurrent = read(idPrevious);
        int index = shopTable.indexOf(shopCurrent);

        shop.setId(shopCurrent.getId());
        shopTable.set(index, shop);
    }

    @Override
    public void delete(int id) {
        Shop shoToDelete = read(id);
        if (Objects.nonNull(shoToDelete)) shopTable.remove(shoToDelete);
        else System.out.println("No shop with id : " + id);
    }
}
