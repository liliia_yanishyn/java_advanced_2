package com.logos.lesson17.dao;

import com.logos.lesson17.model.Shop;

import java.util.List;

public interface ShopDao {

    List<Shop> readAll();

    Shop read(int id);

    void create(Shop shop);

    void update(int idPrevious, String property, String newValue);

    void updateFull(int idPrevious, Shop shop);

    void delete(int id);
}
