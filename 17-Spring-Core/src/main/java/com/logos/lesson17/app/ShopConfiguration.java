package com.logos.lesson17.app;


import com.logos.lesson17.dao.ShopDao;
import com.logos.lesson17.dao.impl.ShopDaoImpl;
import com.logos.lesson17.model.Shop;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class ShopConfiguration {

    @Bean(name = "shopDao")
    @Scope("singleton")
    public ShopDao getShopDao() {
        return new ShopDaoImpl();
    }

    @Bean(name = "shop1")
    public Shop shop1() {
        return new Shop(1, "Daily products", 125.23, 12);
    }

    @Bean(name = "shop2")
    public Shop shop2() {
        return new Shop(2, "Wine time", 253.2, 18);
    }

    @Bean(name = "shop3")
    public Shop shop3() {
        return new Shop(3, "Silpo", 458.23, 45);
    }
}
