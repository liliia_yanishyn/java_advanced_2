package com.logos.lesson17.app;

import com.logos.lesson17.dao.impl.ShopDaoImpl;
import com.logos.lesson17.model.Shop;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);

        ShopDaoImpl shopDao = (ShopDaoImpl) context.getBean("shopDao");
        Shop shop1 = (Shop) context.getBean("shop1");
        Shop shop2 = (Shop) context.getBean("shop2");
        Shop shop3 = (Shop) context.getBean("shop3");

        //C - create
        shopDao.create(shop1);
        shopDao.create(shop2);
        shopDao.create(shop3);

        //R - read by id
        //System.out.println(shopDao.read(1));
        //System.out.println(shopDao.read(11));

        //U - update
        /*shopDao.update(1, "name", "Updated");
        shopDao.update(2, "square", "0.01");
        shopDao.update(3, "maxPeople", "0");
        shopDao.update(3, "test", "0");
        shopDao.update(33, "maxPeople", "0");*/

        //full update
        //shopDao.updateFull(2, new Shop(25, "Elite", 159.23, 14));

        //D - delete
        //shopDao.delete(shop2.getId());

        //R - read all
        //shopDao.readAll().forEach(System.out::println);
    }

}
