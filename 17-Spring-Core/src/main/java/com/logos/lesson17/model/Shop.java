package com.logos.lesson17.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Shop {

    private int id;
    private String name;
    private double square;
    private int maxPeople;
}
