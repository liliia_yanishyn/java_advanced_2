package com.logos.lesson18;

import com.logos.lesson18.exception.AlreadyExistsException;
import com.logos.lesson18.exception.EntityDoesNotExistsException;
import com.logos.lesson18.model.Shop;
import com.logos.lesson18.service.ShopService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Application {

    public static void main(String[] args) throws AlreadyExistsException, EntityDoesNotExistsException {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        ShopService service = context.getBean(ShopService.class);

        Shop shop1 = new Shop(1, "Daily products", 125.23, 12);
        Shop shop2 = new Shop(2, "Wine time", 253.2, 18);
        Shop shop3 = new Shop(3, "Silpo", 458.23, 45);

        //C - create
		/*service.save(shop1);
		service.save(shop2);
		service.save(shop3);*/

        //R - read
        //service.findAll().forEach(System.out::println);
        //System.out.println(service.findById(2));
        //System.out.println(service.findById(15));
        //service.findByName("fvbdgbfv").forEach(System.out::println);
        //service.findAllByMaxPeopleLessThanEqual(20).forEach(System.out::println);

        //U - update
		/*Shop shop = service.findById(2);
		shop.setName("Wine time - UPDATED");
		shop.setMaxPeople(20);
		service.update(shop);*/
        //service.update(new Shop(1, "DAILY" , 123.45, 67));
        //service.update(new Shop(101, "DAILY" , 123.45, 67));

        //D - delete
        //service.deleteById(4);
        //service.deleteById(4);
    }

}
