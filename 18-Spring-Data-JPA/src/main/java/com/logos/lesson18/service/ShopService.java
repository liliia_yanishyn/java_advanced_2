package com.logos.lesson18.service;

import com.logos.lesson18.exception.AlreadyExistsException;
import com.logos.lesson18.exception.EntityDoesNotExistsException;
import com.logos.lesson18.model.Shop;

import java.util.List;

public interface ShopService {

    Shop save(Shop shop) throws AlreadyExistsException;

    Shop findById(int id);

    List<Shop> findAll();

    Shop update(Shop shop) throws EntityDoesNotExistsException;

    void deleteById(int id);

    List<Shop> findByName(String name);

    List<Shop> findAllByMaxPeopleLessThanEqual(int maxPeople);
}
