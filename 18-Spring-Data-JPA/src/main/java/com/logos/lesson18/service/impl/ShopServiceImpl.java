package com.logos.lesson18.service.impl;

import com.logos.lesson18.exception.AlreadyExistsException;
import com.logos.lesson18.exception.EntityDoesNotExistsException;
import com.logos.lesson18.model.Shop;
import com.logos.lesson18.repository.ShopRepository;
import com.logos.lesson18.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShopServiceImpl implements ShopService {

    @Autowired
    private ShopRepository repository;

    @Override
    public Shop save(Shop shop) throws AlreadyExistsException {
        if (repository.existsById(shop.getId())) {
            throw new AlreadyExistsException("Shop : " + shop + " already exists");
        }
        return repository.save(shop);
    }

    @Override
    public Shop findById(int id) {
        //return repository.findById(id).orElseThrow(NullPointerException::new);
        return repository.getOne(id);
    }

    @Override
    public List<Shop> findAll() {
        return repository.findAll();
    }

    @Override
    public Shop update(Shop shop) throws EntityDoesNotExistsException {
        if (repository.existsById(shop.getId())) {
            return repository.save(shop);
        } else {
            throw new EntityDoesNotExistsException("Entity : " + shop + " does not exists!");
        }
    }

    @Override
    public void deleteById(int id) {
        repository.deleteById(id);
    }

    @Override
    public List<Shop> findByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public List<Shop> findAllByMaxPeopleLessThanEqual(int maxPeople) {
        return repository.findAllByMaxPeopleLessThanEqual(maxPeople);
    }
}
