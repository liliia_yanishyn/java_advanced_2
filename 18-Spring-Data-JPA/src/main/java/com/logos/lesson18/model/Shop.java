package com.logos.lesson18.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "shop")
public class Shop {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "shop_id")
    private int id;

    @Column(name = "shop_name")
    private String name;

    @Column(name = "square")
    private double square;

    @Column(name = "max_people")
    private int maxPeople;
}
