package com.logos.lesson18.exception;

public class EntityDoesNotExistsException extends Exception {

    public EntityDoesNotExistsException(String message) {
        super(message);
    }
}
