package com.logos.service.impl;

import model.User;
import com.logos.service.UserService;
import util.MySQLConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UserServiceImpl implements UserService {

    private static Connection connection;
    private static final String CREATE_USER = "INSERT INTO user (last_name, first_name, email, password) VALUES (?, ?,?, ?)";

    static {
        try {
            connection = MySQLConnector.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create(User user) {
        try (PreparedStatement statement = connection.prepareStatement(CREATE_USER)) {
            statement.setString(1, user.getLastName());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPassword());
            statement.execute();
            System.out.println("User was added : " + user);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
