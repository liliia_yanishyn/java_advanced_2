package com.logos.service;

import model.User;

public interface UserService {

    void create(User user);
}
