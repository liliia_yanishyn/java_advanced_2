<%--
  Created by IntelliJ IDEA.
  User: l.yanishyn
  Date: 10/2/2020
  Time: 6:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration page</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>

<h3>Registration here : </h3>
<form action="/registration" method="post">
    <label>First name : </label>
    <input type="text" name="firstName"><br>
    <label>Last name : </label>
    <input type="text" name="lastName"><br>
    <label>Email : </label>
    <input type="email" name="email"><br>
    <label>Password : </label>
    <input type="password" name="password"><br>
    <input type="submit" value="OKI">
</form>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
