package com.logos.lesson21.controller;

import com.logos.lesson21.model.Book;
import com.logos.lesson21.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {

    @Autowired
    private BookRepository repository;

    @GetMapping("/books")
    List<Book> findAll() {
        return repository.findAll();
    }

    @GetMapping("/books/{id}")
    Book findById(@PathVariable("id") int id) {
        return repository.findById(id).orElse(null);
    }

    @PostMapping("/books")
    Book createBook(@RequestBody Book book) {
        return repository.save(book);
    }

    @PutMapping("/books/{id}")
    Book updateBookOsSaveNew(@RequestBody Book newBook, @PathVariable("id") int id) {
        return repository.findById(id)
                .map(book -> {
                    book.setAuthor(newBook.getAuthor());
                    book.setName(newBook.getName());
                    book.setPrice(newBook.getPrice());
                    return repository.save(book);
                })
                .orElseGet(() -> {
                    newBook.setId(id);
                    return repository.save(newBook);
                });
    }

    @DeleteMapping("/books/{id}")
    void deleteById(@PathVariable("id") int id) {
        repository.deleteById(id);
    }
}
