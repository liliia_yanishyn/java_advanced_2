package com.logos.lesson19.repository;

import com.logos.lesson19.model.Shop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ShopRepository extends JpaRepository<Shop, Integer>, CrudRepository<Shop, Integer> {

    List<Shop> findByName(String name);

    List<Shop> findAllByMaxPeopleLessThanEqual(int maxPeople);
}
