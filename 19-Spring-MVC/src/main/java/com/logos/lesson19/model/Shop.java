package com.logos.lesson19.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "shop")
public class Shop {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "shop_id")
    private int id;

    @Column(name = "shop_name")
    @NotBlank
    @Size(min = 5, max = 35, message = "Shop name must contains min 5 characters and max 35")
    private String name;

    @Column(name = "square")
    @Positive
    private double square;

    @Column(name = "max_people")
    @Min(value = 2, message = "Min people value must be more than 2")
    @Max(value = 200, message = "Max people value must be less than 200")
    private int maxPeople;
}
