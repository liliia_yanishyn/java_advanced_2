package com.logos.lesson19.exception;

public class EntityDoesNotExistsException extends Exception {

    public EntityDoesNotExistsException(String message) {
        super(message);
    }
}
