package com.logos.lesson19.cotroller;

import com.logos.lesson19.exception.AlreadyExistsException;
import com.logos.lesson19.exception.EntityDoesNotExistsException;
import com.logos.lesson19.model.Shop;
import com.logos.lesson19.service.impl.ShopServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Objects;

import static javax.servlet.http.HttpServletResponse.*;

@Slf4j
@RestController
public class ShopController {

    @Autowired
    private ShopServiceImpl shopService;

    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = "Successful"),
            @ApiResponse(code = SC_BAD_REQUEST, message = "Error in request creation, validate you request"),
            @ApiResponse(code = SC_NOT_FOUND, message = "No records in database"),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal server error - something wrong with connection or server")
    })
    @ApiOperation(value = "Retrieves information about all shops", response = Shop.class, responseContainer = "List")
    @GetMapping("/shops")
    public ResponseEntity<List<Shop>> getAllShops() {
        log.info("Looking for all shops");
        List<Shop> shops = shopService.findAll();
        if (shops.isEmpty()) {
            log.error("No records found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(shops, HttpStatus.OK);
    }

    @GetMapping("/shops/by-name/{name}")
    public ResponseEntity<List<Shop>> getAllShopsByName(@PathVariable("name") String name) {
        log.info("Looking for all shops by name : {} ", name);
        List<Shop> shops = shopService.findByName(name);
        if (shops.isEmpty()) {
            log.error("No records found");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(shops, HttpStatus.OK);
    }

    @GetMapping("/shops/{id}")
    public ResponseEntity<Shop> getShopById(@PathVariable("id") int id) {
        log.info("Looking for a shop with id : {}", id);
        Shop shop = shopService.findById(id);
        if (Objects.isNull(shop)) {
            log.error("No shop with id : {}", id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(shop, HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(code = SC_CREATED, message = "Shop was created"),
            @ApiResponse(code = SC_BAD_REQUEST, message = "Error in request creation, validate you request"),
            @ApiResponse(code = SC_CONFLICT, message = "Shop with this id already exists"),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Check your fields! OR something wrong with connection or server")
    })
    @ApiOperation(value = "Endpoint to create new shop")
    @PostMapping("/shops")
    public ResponseEntity<?> addNewShop(@RequestBody Shop shop, UriComponentsBuilder builder) throws AlreadyExistsException {
        if (shopService.isExists(shop.getId())) {
            log.error("Shop {} with id {} already exists!", shop, shop.getId());
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        shopService.save(shop);
        log.info("Shop {} was added", shop);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(builder.path("/shops/{id}").buildAndExpand(shop.getId()).toUri());
        return new ResponseEntity<>(httpHeaders, HttpStatus.CREATED);
    }

    @PutMapping("/shops/{id}")
    public ResponseEntity<Shop> updateShopById(@RequestBody Shop shop, @PathVariable("id") int id) throws AlreadyExistsException, EntityDoesNotExistsException {
        if (!shopService.isExists(shop.getId())) {
            log.error("Shop {} with id {} doesn`t exists!", shop, shop.getId());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Shop previous = shopService.findById(id);
        shop.setId(id);
        shopService.update(shop);

        log.info("Shop {} was updated into shop {}", previous, shop);
        return new ResponseEntity<>(shop, HttpStatus.OK);
    }

    @DeleteMapping("/shops/{id}")
    public ResponseEntity<?> deleteShopById(@PathVariable("id") int id) throws AlreadyExistsException, EntityDoesNotExistsException {
        if (Objects.isNull(shopService.findById(id))) {
            log.error("Shop with id {} doesn`t exists!", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        shopService.deleteById(id);

        log.info("Shop with id {} was deleted", id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
