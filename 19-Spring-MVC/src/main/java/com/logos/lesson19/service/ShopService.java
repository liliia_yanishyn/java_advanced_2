package com.logos.lesson19.service;


import com.logos.lesson19.exception.AlreadyExistsException;
import com.logos.lesson19.exception.EntityDoesNotExistsException;
import com.logos.lesson19.model.Shop;

import java.util.List;

public interface ShopService {

    Shop save(Shop shop) throws AlreadyExistsException;

    Shop findById(int id);

    List<Shop> findAll();

    Shop update(Shop shop) throws EntityDoesNotExistsException;

    void deleteById(int id);

    List<Shop> findByName(String name);

    List<Shop> findAllByMaxPeopleLessThanEqual(int maxPeople);

    boolean isExists(int id);
}
