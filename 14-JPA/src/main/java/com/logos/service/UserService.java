package com.logos.service;

import model.User;

import java.util.List;

public interface UserService {

    User getUserByEmail(String email);

    User read(int id);

    List<User> readAll();

    void create(User user);

    void delete(int id);
}
