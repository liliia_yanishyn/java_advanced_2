import many_to_many.Bucket;
import many_to_many.Product;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.logos.service.PersonService;
import com.logos.service.imp.PersonServiceImpl;
import util.SessionFactoryUtil;

import java.util.Arrays;
import java.util.HashSet;

public class Application {

    private static final PersonService personService = new PersonServiceImpl();

    public static void main(String[] args) {
        //personService.create(new Person(2, "Anna", "Lalalla", 25));
        //personService.create(new Person( "Nazar", "Pipipi", 45));

        //System.out.println(personService.read(2));
        //personService.readAll().forEach(System.out::println);

        //personService.update(1, new Person("Oleg", "HKbfhfv", 21));
        //personService.delete(1);
        //personService.readAll().forEach(System.out::println);

        Session session = SessionFactoryUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Bucket bucket1 = new Bucket("bucket-1");
        Product p1 = new Product("Apple", 25);
        Product p2 = new Product("Orange", 12);
        Product p3 = new Product("Banana", 14);

        bucket1.setProducts(new HashSet<>(Arrays.asList(p1, p2, p3)));

        Bucket bucket2 = new Bucket("bucket-2");
        Product p4 = new Product("Bread", 23);
        Product p5 = new Product("Coffee", 122);
        Product p6 = new Product("Tea", 45);

        bucket2.setProducts(new HashSet<>(Arrays.asList(p4, p5, p6)));

        session.persist(bucket1);
        session.persist(bucket2);

        transaction.commit();
        session.close();

    }
}
