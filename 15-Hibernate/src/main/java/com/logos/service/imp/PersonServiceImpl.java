package com.logos.service.imp;

import com.logos.service.PersonService;
import model.Person;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.SessionFactoryUtil;

import java.util.List;

public class PersonServiceImpl implements PersonService {

    private Session session;
    private Transaction transaction;

    public PersonServiceImpl() {
        session = SessionFactoryUtil.getSession();
        transaction = session.beginTransaction();
    }

    @Override
    public Person read(int id) {
        return session.get(Person.class, id);
    }

    @Override
    public List<Person> readAll() {
        return session.createCriteria(Person.class).list();
    }

    @Override
    public void create(Person person) {
        session.persist(person);
        transaction.commit();
    }

    @Override
    public void update(int previousId, Person update) {
        Person personForUpdate = read(previousId);
        personForUpdate.setFirstName(update.getFirstName());
        personForUpdate.setLastName(update.getLastName());
        personForUpdate.setAge(update.getAge());

        session.persist(personForUpdate);
        transaction.commit();
    }

    @Override
    public void delete(int id) {
        session.delete(read(id));
        transaction.commit();
    }
}
