package com.logos.service;

import model.Person;

import java.util.List;

public interface PersonService {

    Person read(int id);

    List<Person> readAll();

    void create(Person person);

    void update(int previousId, Person update);

    void delete(int id);

}
